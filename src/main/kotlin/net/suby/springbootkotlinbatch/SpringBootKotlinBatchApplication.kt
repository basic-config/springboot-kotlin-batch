package net.suby.springbootkotlinbatch

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootKotlinBatchApplication

fun main(args: Array<String>) {
    runApplication<SpringBootKotlinBatchApplication>(*args)
}
