package net.suby.springbootkotlinbatch.service

import net.suby.springbootkotlinbatch.properties.CrowdWorkUriInfo
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.nio.charset.StandardCharsets

@Service
class CrowdworkService(val restTemplate : RestTemplate, val crowdWorkUriInfo : CrowdWorkUriInfo) {

    fun getCrowdWork(){
        val intUnixTime = System.currentTimeMillis().toLong() / 1000
        val uri : String = UriComponentsBuilder.newInstance()
                .scheme(crowdWorkUriInfo.scheme)
                .host(crowdWorkUriInfo.endpoint)
                .path(crowdWorkUriInfo.path)
                .queryParam("sort","endDate")
                .queryParam("device","PC")
                .queryParam("_",intUnixTime)
                .build().toUriString()

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        headers.acceptCharset = listOf(StandardCharsets.UTF_8)

        // 보안이슈로 조회가 되지 않음
        val items = this.restTemplate.exchange(uri, HttpMethod.GET, HttpEntity<Any>(null, headers), object : ParameterizedTypeReference<String?>() {})
        println(items)
    }
}