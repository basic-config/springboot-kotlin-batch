package net.suby.springbootkotlinbatch.properties

    import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "uri.crowdwork")
class CrowdWorkUriInfo() {
    var scheme : String = ""
        get() = field
        set(value){ field = value }

    var endpoint : String = ""
        get() = field
        set(value){ field = value }

    var path : String = ""
        get() = field
        set(value){ field = value }
}