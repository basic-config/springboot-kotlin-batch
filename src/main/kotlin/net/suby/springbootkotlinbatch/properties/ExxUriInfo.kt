package net.suby.springbootkotlinbatch.properties

    import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "uri.exx")
class ExxUriInfo() {
    var scheme : String = ""
        get() = field
        set(value){ field = value }

    var endpoint : String = ""
        get() = field
        set(value){ field = value }

    var marketsPath : String = ""
        get() = field
        set(value){ field = value }
}