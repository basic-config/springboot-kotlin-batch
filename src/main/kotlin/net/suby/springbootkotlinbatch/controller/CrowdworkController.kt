package net.suby.springbootkotlinbatch.controller

import net.suby.springbootkotlinbatch.service.CrowdworkService
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/crowds")
class CrowdworkController(val crowdworkService : CrowdworkService) {

    @GetMapping("/work")
    fun getCrowdWork(){
        this.crowdworkService.getCrowdWork()
    }
}