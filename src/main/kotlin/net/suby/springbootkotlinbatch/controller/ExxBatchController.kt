package net.suby.springbootkotlinbatch.controller

import net.suby.springbootkotlinbatch.properties.ExxUriInfo
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@RestController
@RequestMapping("/exx")
class ExxBatchController(val restTemplate : RestTemplate, val exxUriInfo : ExxUriInfo) {

    @GetMapping("/batch")
    fun xeeExecuteBatch(){
        val uri : String = UriComponentsBuilder.newInstance()
                .scheme(exxUriInfo.scheme)
                .host(exxUriInfo.endpoint)
                .path(exxUriInfo.marketsPath)

                .build().toUriString()

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val items = this.restTemplate.exchange(uri, HttpMethod.GET, HttpEntity<Any>(null, headers), object : ParameterizedTypeReference<String?>() {})
        println(items)
    }

}